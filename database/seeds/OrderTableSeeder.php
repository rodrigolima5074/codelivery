<?php
use Codelivery\Entities\Category;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Codelivery\Entities\Order::class, 10)->create()->each( function($o) {
            for($i=0;$i<=5;$i++){
                    $o->items()->save(factory(\Codelivery\Entities\OrderItem::class)->make([
                        'product_id' => rand(1,5),
                        'qtd' => 2,
                        'price' => rand(50,100)
                    ]));
            }
	    });
    }
}
