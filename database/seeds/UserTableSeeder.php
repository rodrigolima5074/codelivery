<?php

use Codelivery\Entities\Client;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Codelivery\Entities\User::class)->create([
            'name' => 'User',
            'email'=> 'user@user.com',
            'password' => bcrypt(123456),
            'remember_token' => str_random('10')
        ])->client()->save(factory(Client::class)->make());

        factory(\Codelivery\Entities\User::class)->create([
            'name' => 'Admin',
            'email'=> 'admin@admin.com',
            'password' => bcrypt(123456),
            'remember_token' => str_random('10'),
            'role' => 'admin',
        ])->client()->save(factory(Client::class)->make());

        factory(\Codelivery\Entities\User::class, 10)->create()->each(function ( $usuario ) {
            $usuario->client()->save(factory(\Codelivery\Entities\Client::class)->make());
        });

        factory(\Codelivery\Entities\User::class, 3)->create([
            'role' => 'deliveryman',
        ]);
    }
}
