<?php

namespace Codelivery\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Order extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'client_id',
        'user_deliverman_id',
        'total',
        'status'
    ];

    /*public function transform()
    {
        return [
            'order' => $this->id
        ];
    }*/

    public function client()
    {
        return  $this->belongsTo(Client::class);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function deliveryman()
    {
        return $this->belongsTo(User::class, 'user_deliverman_id', 'id');
    }

    public function cupom()
    {
        return $this->hasOne(Cupom::class, 'id', 'cupom_id');
    }
}
