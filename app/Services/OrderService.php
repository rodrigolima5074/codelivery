<?php namespace Codelivery\Services;

use Codelivery\Entities\Order;
use Codelivery\Repositories\ClientRepository;
use Codelivery\Repositories\CupomRepository;
use Codelivery\Repositories\OrderRepository;
use Codelivery\Repositories\ProductRepository;
use Codelivery\Repositories\UserRepository;

class OrderService
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var CupomRepository
     */
    private $cupomRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(OrderRepository $orderRepository,
                                CupomRepository $cupomRepository,
                                ProductRepository $productRepository)
    {

        $this->orderRepository = $orderRepository;
        $this->cupomRepository = $cupomRepository;
        $this->productRepository = $productRepository;
    }

    public function update(array $data, $id){

    }

    public function create(array $data)
    {
        \DB::beginTransaction();
        try {
            $data['status'] = 0;
            if (isset($data['cupom_code'])) {
                $cupom = $this->cupomRepository->findByField('code', $data['cupom_code'])->first();
                $data['cupom_id'] = $cupom->id;
                $cupom->used = 1;
                $cupom->save();
                unset($data['cupom_code']);
            }

            $items = $data['items'];
            unset($data['items']);

            $order = $this->orderRepository->create($data);
            $total = 0;

            foreach ($items as $item) {
                $item['price'] = $this->productRepository->find($item['product_id'])->price;
                $order->items()->create($item);
                $total += $item['price'] * $item['qtd'];
            }

            $order->total = $total;
            if (isset($cupom))
                $order->total = $total - $cupom->value;

            $order->save();
            \DB::commit();
            return $order;
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
        }
    }

    public function updateStatus($orderId, $deliverymanId, $status)
    {
        $order = $this->orderRepository->getByIdDeliveryman($orderId, $deliverymanId);
        if ($order instanceof Order) {
            $order->status = $status;
            $order->save();
            return $order;
        }
        return false;
    }
}