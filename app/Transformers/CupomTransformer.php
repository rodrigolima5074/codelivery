<?php namespace Codelivery\Transformers;

use Codelivery\Entities\Cupom;
use League\Fractal\TransformerAbstract;
use Codelivery\Entities\Order;

/**
 * Class OrderTransformer
 * @package namespace Codelivery\Transformers;
 */
class CupomTransformer extends TransformerAbstract
{

    /**
     * Transform the \Order entity
     * @param \Order $model
     *
     * @return array
     */
    public function transform(Cupom $model)
    {
        return [
            'id' => (int)$model->id,
            'code'  => $model->code,
            'value' => (float)$model->value,
            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}