<?php

namespace Codelivery\Transformers;

use Codelivery\Entities\User;
use League\Fractal\TransformerAbstract;
use Codelivery\Entities\Client;

/**
 * Class ClientTransformer
 * @package namespace Codelivery\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['client'];

    /**
     * Transform the \Client entity
     * @param \Client $model
     *
     * @return array
     */
    public function transform(Client $model) {
        return [
            'id'         => (int)$model->id,
            'name'       => $model->name,
            'email'      => $model->email,
            'role'       => $model->role
        ];
    }

    public function includeClient(User $model) {
        return $this->item($model->client, new ClientTransformer());
    }
}