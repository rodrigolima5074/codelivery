<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'auth.checkrole'], function () {
	Route::get('categories', ['as' => 'categories.index', 'uses' => 'CategoriesController@index']);
	Route::get('categories/create', ['as' => 'categories.create', 'uses' => 'CategoriesController@create']);
	Route::get('categories/edit/{id}', ['as' => 'categories.edit', 'uses' => 'CategoriesController@edit']);
	Route::put('categories/update/{id}', ['as' => 'categories.update', 'uses' => 'CategoriesController@update']);
	Route::post('categories/store', ['as' => 'categories.store', 'uses' => 'CategoriesController@store']);

	Route::get('clients', ['as' => 'clients.index', 'uses' => 'ClientsController@index']);
	Route::get('clients/create', ['as' => 'clients.create', 'uses' => 'ClientsController@create']);
	Route::get('clients/edit/{id}', ['as' => 'clients.edit', 'uses' => 'ClientsController@edit']);
	Route::put('clients/update/{id}', ['as' => 'clients.update', 'uses' => 'ClientsController@update']);
	Route::post('clients/store', ['as' => 'clients.store', 'uses' => 'ClientsController@store']);

	Route::get('products', ['as' => 'products.index', 'uses' => 'ProductsController@index']);
	Route::get('products/create', ['as' => 'products.create', 'uses' => 'ProductsController@create']);
	Route::get('products/edit/{id}', ['as' => 'products.edit', 'uses' => 'ProductsController@edit']);
	Route::put('products/update/{id}', ['as' => 'products.update', 'uses' => 'ProductsController@update']);
	Route::get('products/destroy/{id}', ['as' => 'products.destroy', 'uses' => 'ProductsController@destroy']);
	Route::post('products/store', ['as' => 'products.store', 'uses' => 'ProductsController@store']);

	Route::resource('orders', 'OrderController');
	Route::resource('cupoms', 'CupomsController');
});

Route::group(['prefix' => 'customer', 'as' => 'customer.'], function () {
	Route::get('order', ['as' => 'order.index', 'uses' => 'CheckoutController@index']);
	Route::get('order/create', ['as' => 'order.create', 'uses' => 'CheckoutController@create']);
	Route::post('order/store', ['as' => 'order.store', 'uses' => 'CheckoutController@store']);
});

Route::group(['middleware' => 'cors'], function(){

Route::post('oauth/access_token', function () {
	return Response::json(Authorizer::issueAccessToken());
});
});

Route::group(['prefix' => 'api', 'as' => 'api.', 'middleware' => ['oauth','cors']], function () {
	Route::group(['prefix' => 'client', 'as' => 'client.', 'middleware' => 'oauth.checkrole:client'], function () {
		Route::resource('order', 'Api\Client\ClientCheckoutController',
			['except' => ['create', 'edit', 'destroy']]
		);

		//\Codelivery\Http\Controllers\Api\Client\ClientCheckoutController
		Route::get('products', 'Api\Client\ClientProductController@index');

	});

	Route::group(['prefix' => 'deliveryman', 'as' => 'deliveryman.', 'middleware' => 'oauth.checkrole:deliveryman'], function () {
		Route::resource('order', 'Api\Deliveryman\DeliverymanCheckoutController',
				['except' => ['create', 'edit', 'destroy']]
		);
		Route::patch('order/{id}/update-status', [
				'uses' => 'Api\Deliveryman\DeliverymanCheckoutController@updateStatus',
				'as' => 'orders.update.status'
		]);
	});

	Route::get('authenticated', 'Api\UserController@authenticated');
	Route::get('cupom/{code}', 'Api\CupomController@show');
});
