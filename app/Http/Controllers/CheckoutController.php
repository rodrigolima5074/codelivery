<?php

namespace Codelivery\Http\Controllers;

use Codelivery\Repositories\CategoryRepository;
use Codelivery\Repositories\OrderRepository;
use Codelivery\Repositories\ProductRepository;
use Codelivery\Repositories\UserRepository;
use Codelivery\Services\OrderService;
use Illuminate\Http\Request;
use Codelivery\Http\Requests;
use Codelivery\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class CheckoutController extends Controller
{

    /**
     * @var CategoryRepository
     */
    private $orderRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var OrderService
     */
    private $orderService;

    public function __construct(OrderRepository $orderRepository,
                                UserRepository $userRepository,
                                ProductRepository $productRepository,
                                OrderService $orderService)
    {
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->orderService = $orderService;
    }

    public function index()
    {
        $clientId = $this->userRepository->find(Auth::user()->id)->client->id;
        $orders = $this->orderRepository->scopeQuery(function($q) use ($clientId){
            return $q->where('client_id', '=', $clientId);
        })->paginate();

        return view('customer.order.index', compact('orders'));
    }

    public function create()
    {
        $products = $this->productRepository->lists();
        return view('customer.order.create', compact('products'));
    }

    public function store(Requests\CheckoutRequest $request)
    {
        $data = $request->all();
        $clientId = $this->userRepository->find(Auth::user()->id)->client->id;
        $data['client_id'] = $clientId;

        $this->orderService->create($data);

        return redirect()->route('customer.order.index');
    }
}
