<?php namespace Codelivery\Http\Controllers\Api\Client;

use Codelivery\Repositories\CategoryRepository;
use Codelivery\Repositories\OrderRepository;
use Codelivery\Repositories\ProductRepository;
use Codelivery\Repositories\UserRepository;
use Codelivery\Services\OrderService;
use Illuminate\Http\Request;
use Codelivery\Http\Requests;
use Codelivery\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class ClientCheckoutController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $orderRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var OrderService
     */
    private $orderService;

    private $with = ['client', 'cupom', 'items'];

    public function __construct(OrderRepository $orderRepository,
                                UserRepository $userRepository,
                                ProductRepository $productRepository,
                                OrderService $orderService)
    {
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->orderService = $orderService;
    }

    public function index()
    {
        $id = Authorizer::getResourceOwnerId();
        $clientId = $this->userRepository->find($id)->client->id;
        $orders = $this->orderRepository->skipPresenter(false)->with($this->with)->scopeQuery(function($q) use ($clientId){
            return $q->where('client_id', '=', $clientId);
        })->paginate();

        return $orders;
    }

    public function store(Requests\CheckoutRequest $request)
    {
        $data = $request->all();
        $id = Authorizer::getResourceOwnerId();
        $clientId = $this->userRepository->find($id)->client->id;
        $data['client_id'] = $clientId;
        $order = $this->orderService->create($data);
        $order = $this->orderRepository->with($this->with)->find($order->id);
        return $order;
    }

    public function show($id)
    {
        $o = $this->orderRepository->skipPresenter(false)->with($this->with)->find($id);
        /*$o->items->each(function($item){
           $item->product;
        });*/
        return $o;
    }
}
