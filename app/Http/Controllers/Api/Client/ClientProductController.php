<?php namespace Codelivery\Http\Controllers\Api\Client;

use Codelivery\Http\Controllers\Controller;
use Codelivery\Repositories\ProductRepository;

class ClientProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;

    }

    public function index()
    {
        $products = $this->productRepository->skipPresenter(false)->all();
        return $products;
    }

}
