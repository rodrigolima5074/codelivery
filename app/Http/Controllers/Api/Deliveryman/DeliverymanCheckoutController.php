<?php namespace Codelivery\Http\Controllers\Api\Deliveryman;

use Codelivery\Repositories\CategoryRepository;
use Codelivery\Repositories\OrderRepository;
use Codelivery\Repositories\ProductRepository;
use Codelivery\Repositories\UserRepository;
use Codelivery\Services\OrderService;
use Illuminate\Http\Request;
use Codelivery\Http\Requests;
use Codelivery\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class DeliverymanCheckoutController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $orderRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var OrderService
     */
    private $orderService;

    protected $with = [];

    public function __construct(OrderRepository $orderRepository,
                                UserRepository $userRepository,
                                OrderService $orderService)
    {
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->orderService = $orderService;
    }

    public function index()
    {
        $id = Authorizer::getResourceOwnerId();
        $orders = $this->orderRepository->with(['items'])->scopeQuery(function($q) use ($id){
            return $q->where('user_deliverman_id', '=', $id);
        })->paginate();

        return $orders;
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $id = Authorizer::getResourceOwnerId();
        $clientId = $this->userRepository->find($id)->client->id;
        $data['client_id'] = $clientId;
        $order = $this->orderService->create($data);
        $order = $this->orderRepository->with('items')->find($order->id);
        return $order;
    }

    public function show($id)
    {
        $deliverymanId = Authorizer::getResourceOwnerId();
        return $this->orderRepository->getByAndDeliveryman($id, $deliverymanId);
    }

    public function updateStatus(Request $request, $id)
    {

        $deliverymanId = Authorizer::getResourceOwnerId();
        $order = $this->orderService->updateStatus($id, $deliverymanId, $request->get('status'));
        if ($order)
            return $order;

        abort(400, "Order não encontrado.");

    }
}

