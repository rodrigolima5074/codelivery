<?php namespace Codelivery\Http\Controllers\Api;

use Codelivery\Http\Controllers\Controller;
use Codelivery\Repositories\CupomRepository;
use Codelivery\Repositories\ProductRepository;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class UserController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

    }

    public function authenticated()
    {
        $id = Authorizer::getResourceOwnerId();
        return $this->userRepository->skipPresenter(false)->find($id);
    }

}
