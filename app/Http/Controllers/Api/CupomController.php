<?php namespace Codelivery\Http\Controllers\Api;

use Codelivery\Http\Controllers\Controller;
use Codelivery\Repositories\CupomRepository;
use Codelivery\Repositories\ProductRepository;

class CupomController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $cupomRepository;

    public function __construct(CupomRepository $cupomRepository)
    {
        $this->cupomRepository = $cupomRepository;

    }

    public function show($code)
    {
        return $this->cupomRepository->skipPresenter(false)->findByCode($code);
    }

}
