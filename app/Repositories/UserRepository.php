<?php

namespace Codelivery\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace Codelivery\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
