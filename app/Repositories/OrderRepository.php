<?php

namespace Codelivery\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderRepository
 * @package namespace Codelivery\Repositories;
 */
interface OrderRepository extends RepositoryInterface
{
    //
}
