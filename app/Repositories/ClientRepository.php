<?php

namespace Codelivery\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ClientRepository
 * @package namespace Codelivery\Repositories;
 */
interface ClientRepository extends RepositoryInterface
{
    //
}
