<?php

namespace Codelivery\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Codelivery\Repositories\ProductRepository;
use Codelivery\Presenters\ProductPresenter;
use Codelivery\Entities\Product;

/**
 * Class ProductRepositoryEloquent
 * @package namespace Codelivery\Repositories;
 */
class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{

    protected $skipPresenter = true;

    public function lists()
    {
        return $this->model->get(['id', 'name', 'price']);
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function presenter()
    {
        return ProductPresenter::class; // TODO: Change the autogenerated stub
    }
}
