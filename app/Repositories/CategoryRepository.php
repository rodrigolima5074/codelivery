<?php

namespace Codelivery\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository
 * @package namespace Codelivery\Repositories;
 */
interface CategoryRepository extends RepositoryInterface
{
    //
}
