<?php

namespace Codelivery\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Codelivery\Repositories\UserRepository;
use Codelivery\Entities\User;
use Codelivery\Presenters\UserPresenter;

/**
 * Class UserRepositoryEloquent
 * @package namespace Codelivery\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    private $skipPresenter = true;

    public function getDeliverymen()
    {
        return $this->model->where(['role' => 'deliveryman'])->lists('name', 'id');
    }

    public function presenter()
    {
        return UserPresenter::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
