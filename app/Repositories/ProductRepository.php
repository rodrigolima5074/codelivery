<?php

namespace Codelivery\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository
 * @package namespace Codelivery\Repositories;
 */
interface ProductRepository extends RepositoryInterface
{
    //
}
