<?php

namespace Codelivery\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CupomRepository
 * @package namespace Codelivery\Repositories;
 */
interface CupomRepository extends RepositoryInterface
{
    public function findByCode($code);
}
