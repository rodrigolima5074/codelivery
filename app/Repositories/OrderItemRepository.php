<?php

namespace Codelivery\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderItemRepository
 * @package namespace Codelivery\Repositories;
 */
interface OrderItemRepository extends RepositoryInterface
{
    //
}
