<?php

namespace Codelivery\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Codelivery\Repositories\OrderRepository;
use Codelivery\Entities\Order;

/**
 * Class OrderRepositoryEloquent
 * @package namespace Codelivery\Repositories;
 */
class OrderRepositoryEloquent extends BaseRepository implements OrderRepository
{
    protected $skipPresenter = true;

    public function presenter()
    {
        return \Codelivery\Presenters\OrderPresenter::class;
    }

    public function getByAndDeliveryman($orderId, $deliverymanId)
    {
        $r = $this->with(['client','items'])->findWhere([
            'id' => $orderId,
            'user_deliverman_id' => $deliverymanId
        ]);

        if ($r instanceof Collection)
            $r = $r->first()->items->each(function($item){
                $item->product;
            });

        return $r;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Order::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
