<?php

namespace Codelivery\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Codelivery\Repositories\CategoryRepository',
            'Codelivery\Repositories\CategoryRepositoryEloquent'
        );

        $this->app->bind(
            'Codelivery\Repositories\ProductRepository',
            'Codelivery\Repositories\ProductRepositoryEloquent'
        );

        $this->app->bind(
            'Codelivery\Repositories\ClientRepository',
            'Codelivery\Repositories\ClientRepositoryEloquent'
        );

        $this->app->bind(
            'Codelivery\Repositories\UserRepository',
            'Codelivery\Repositories\UserRepositoryEloquent'
        );

        $this->app->bind(
            'Codelivery\Repositories\OrderRepository',
            'Codelivery\Repositories\OrderRepositoryEloquent'
        );

        $this->app->bind(
            'Codelivery\Repositories\CupomRepository',
            'Codelivery\Repositories\CupomRepositoryEloquent'
        );
    }
}
