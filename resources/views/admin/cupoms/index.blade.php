@extends('app')

@section('content')
    <div class="container">
        <h3>Cupoms</h3>
        <a href="{{route('admin.admin.cupoms.create')}}" class="btn btn-default">Nov Cupom</a>
        <br/>
        <br/>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>id</th>
                    <th>code</th>
                    <th>value</th>
                    <th>used</th>
                    <th>acao</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cupoms as $cupom)
                    <tr>
                        <td>{!! $cupom->id !!}</td>
                        <td>{!! $cupom->code !!}</td>
                        <td>{!! $cupom->value !!}</td>
                        <td>{!! $cupom->used !!}</td>
                        <td><a href="{{route('admin.admin.cupoms.edit', ['id' => $cupom->id])}}" class="btn btn-default">editar</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {!! $cupoms->render() !!}
    </div>
@endsection