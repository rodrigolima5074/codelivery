@extends('app')

@section('content')
    <div class="container">
        <h3>Produtos</h3>
        <a href="{{route('admin.products.create')}}" class="btn btn-default">Novo Produto</a>
        <br/>
        <br/>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>id</th>
                    <th>nome</th>
                    <th>categoria</th>
                    <th>preço</th>
                    <th>acao</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{!! $product->id !!}</td>
                        <td>{!! $product->name !!}</td>
                        <td>{!! $product->category->name !!}</td>
                        <td>{!! $product->price !!}</td>
                        <td>
                            <a href="{{route('admin.products.edit', ['id' => $product->id])}}" class="btn btn-default">editar</a>
                            <a href="{{route('admin.products.destroy', ['id' => $product->id])}}" class="btn btn-danger">excluir</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {!! $products->render() !!}
    </div>
@endsection