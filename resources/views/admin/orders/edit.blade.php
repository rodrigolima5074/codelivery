@extends('app')

@section('content')
    <div class="container">
        <h2>Pedido: #{{$order->id}} - R${{$order->total}}</h2>
        <h3>Cliente: {{$order->client->user->name}}</h3>
        <h4>Data: {{$order->created_at}}</h4>

        <p>
            <b>Etregar em:</b> <br/>
            {{$order->client->address}} - {{$order->client->city}} - {{$order->client->state}}
        </p>
        @include('errors.check')
        {!! Form::model($order, ['route' => ['admin.admin.orders.update', $order->id], 'method' => 'put']) !!}
            @include('admin.orders.form')

            <div class="form-group">
                {!! Form::submit('Salvar Order', ['class' => 'btn btn-primary']) !!}
            </div>

        {!! Form::close() !!}
    </div>
@endsection