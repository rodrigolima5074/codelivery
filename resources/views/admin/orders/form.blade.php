<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', $list_status, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('deliveryman_id', 'Entregador:') !!}
    {!! Form::select('user_deliverman_id', $deliveryman, null, ['class' => 'form-control']) !!}
</div>